package top.lethelh.pattern.factory.before;

/**
 * 美式咖啡
 */
public class AmericanCoffee extends Coffee{
    @Override
    public String getName() {
        return "美式咖啡";
    }
}
