package top.lethelh.pattern.factory.abstract_factory;

/**
 * 甜品抽象
 */
public abstract class Dessert {

    public abstract void show();
}
