package top.lethelh.pattern.factory.simple_factory;

/**
 * 咖啡店
 */
public class CoffeeStore {

    public Coffee orderCoffee(String type)
    {
        SimpleCoffeeFactory factory = new SimpleCoffeeFactory();
        Coffee coffee = factory.createCoffee(type);
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
