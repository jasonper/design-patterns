package top.lethelh.pattern.adapter.object_adapter;

/**
 * 计算机类
 */
public class Computer {

    // 从SD卡里读取数据
    public String readSD(SDCard sdCard) {
        if (sdCard == null) {
            throw new NullPointerException("Sd card is null");
        }
        String msg = sdCard.readSD();
        return msg;
    }
}
