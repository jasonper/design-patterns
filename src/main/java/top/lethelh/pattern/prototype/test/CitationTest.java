package top.lethelh.pattern.prototype.test;

public class CitationTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        // 1.创建原型对象
        Citation citation = new Citation();
        // 创建张三对象
        Student stu = new Student();
        stu.setName("张三");
        citation.setStu(stu);
        // 2.复制克隆奖状对象
        Citation citation1 = citation.clone();
        citation1.getStu().setName("李四");

        // citation.setName("张三");
        // citation1.setName("李四");

        // 3.调用show()展示奖状
        citation.show();
        citation1.show();
    }
}
