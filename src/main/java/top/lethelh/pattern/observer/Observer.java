package top.lethelh.pattern.observer;

/**
 * 抽象观察者接口
 */
public interface Observer {

    void update(String message);
}
