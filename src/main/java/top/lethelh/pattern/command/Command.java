package top.lethelh.pattern.command;

/**
 * 抽象命令类
 */
public interface Command {

    void execute();
}
