package top.lethelh.pattern.command;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务员类: 请求者角色
 */
public class Waitor {
    // 持有多个命令对象
    private List<Command> commands = new ArrayList<>();

    public void setCommand(Command cmd) {
        // 将cmd对象存储到List集合中
        commands.add(cmd);
    }

    // 发起命令功能 喊订单来了
    public void orderUp() {
        System.out.println("美女服务员说:大厨,新订单来了");
        // 遍历List集合
        for (Command command : commands) {
            if (command != null) {
                command.execute();
            }
        }
    }
}
