package top.lethelh.pattern.responsibility.jdk;

public interface Filter {

    void doFilter(Req req, Resp resp, FilterChain chain);
}
