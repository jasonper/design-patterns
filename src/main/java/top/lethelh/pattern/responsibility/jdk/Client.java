package top.lethelh.pattern.responsibility.jdk;

/**
 * 测试
 */
public class Client {

    public static void main(String[] args) {
        Req  req = null;
        Resp res = null ;

        FilterChain filterChain = new FilterChain();
        filterChain.addFilter(new FirstFilter()).addFilter(new SecondFilter());
        filterChain.doFilter(req, res);
    }
}
