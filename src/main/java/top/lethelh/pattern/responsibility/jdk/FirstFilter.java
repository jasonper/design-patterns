package top.lethelh.pattern.responsibility.jdk;

public class FirstFilter implements Filter{
    @Override
    public void doFilter(Req req, Resp resp, FilterChain chain) {
        System.out.println("过滤器1 前置处理");
        // 先执行所有request再倒序执行所有response
        chain.doFilter(req, resp);
        System.out.println("过滤器1 后置处理");
    }
}
