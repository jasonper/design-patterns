package top.lethelh.pattern.facade;

/**
 * 外观模式测试类
 */
public class Client {

    public static void main(String[] args) {
        // 创建一个智能音箱对象
        SmartAppliancesFacade facade = new SmartAppliancesFacade();
        // 控制家电
        facade.say("打开家电");

        System.out.println("================");

        facade.say("关闭家电");
    }
}
