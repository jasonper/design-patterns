package top.lethelh.pattern.singleton.demo06;

/**
 * 饿汉式:枚举方式(不考虑内存浪费)
 */
public enum Singleton {
    INSTANCE
}
