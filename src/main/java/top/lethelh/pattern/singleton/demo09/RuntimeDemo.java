package top.lethelh.pattern.singleton.demo09;

import java.io.IOException;
import java.io.InputStream;

public class RuntimeDemo {

    public static void main(String[] args) throws IOException {
        // 获取Runtime类的对象
        Runtime runtime = Runtime.getRuntime();

        // 调用Runtime的exec方法,参数的是一个命令
        Process process = runtime.exec("ipconfig");
        // 调用process对象的获取输入流的方法 打印到控制台
        InputStream is = process.getInputStream();
        byte[] arr = new byte[1024 * 1024 * 100];
        // 读取数据
        int len = is.read(arr); // 返回读到的字节字数
        // 将字节数组转换为字符串输出到控制台
        System.out.println(new String(arr, 0, len, "GBK"));
    }
}
