package top.lethelh.pattern.singleton.demo02;

/**
 * 饿汉式:静态代码块的方式
 */
public class Singleton {

    // 1.私有构造方法
    private Singleton() {}

    // 2.声明Singleton类型的变量
    private static Singleton instance;

    // 3.在静态代码块中进行赋值
    static {
        instance = new Singleton();
    }

    // 4.对外提供获取该类对象的方法
    public static Singleton getInstance() {
        return instance;
    }
}
