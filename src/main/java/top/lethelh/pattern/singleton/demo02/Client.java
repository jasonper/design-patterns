package top.lethelh.pattern.singleton.demo02;

public class Client {

    public static void main(String[] args) {
        Singleton instance1 = Singleton.getInstance();
        Singleton instance2 = Singleton.getInstance();

        // 判断获取到的两个对象是否是同一个对象
        System.out.println(instance1 == instance2);
    }
}
