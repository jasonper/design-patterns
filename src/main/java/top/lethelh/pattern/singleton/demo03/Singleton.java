package top.lethelh.pattern.singleton.demo03;

/**
 * 懒汉式:线程不安全与线程安全
 */
public class Singleton {

    // 1.私有构造方法
    private Singleton() {}

    // 2.声明Singleton类型的变量instance
    private static Singleton instance; // 只是声明了一个该类型的变量,并没有进行赋值

    // 3.对外提供访问方式
    // 加入synchronized 就线程安全了
    public static synchronized Singleton getInstance() {
        // 判断instance是否为null,如果为null,说明还没有创建Singleton类的对象
        // 如果没有,创建一个并返回;如果有,直接返回
        if (instance == null) {
            // 线程1等待,线程2得到cpu的执行权,也会进入到该判断里面
            instance = new Singleton();
        }
        return instance;
    }
}
