package top.lethelh.pattern.singleton.demo08;

import java.lang.reflect.Constructor;

/**
 * 测试使用反射破坏单例模式
 */
public class Client {

    public static void main(String[] args) throws Exception {
        // 1.获取Singleton的字节码对象
        Class clazz = Singleton.class;
        // 2.获取无参构造方法对象
        Constructor cons = clazz.getDeclaredConstructor();
        // 3.取消访问检查
        cons.setAccessible(true);
        // 4.创建Singleton对象
        Singleton instance1 = (Singleton) cons.newInstance();
        Singleton instance2 = (Singleton) cons.newInstance();

        // 如果返回的是true,表示并没有破坏单例模式;如果是false,则说明破坏了单例模式
        System.out.println(instance1 == instance2);
    }
}
