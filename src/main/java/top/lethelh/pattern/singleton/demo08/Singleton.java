package top.lethelh.pattern.singleton.demo08;

import java.io.Serializable;

/**
 * 懒汉式:静态内部类方式
 */
public class Singleton implements Serializable {

    private static boolean flag = false;

    /**
     * 解决序反射造成的 破坏单例模式
     */
    // 1.私有构造方法
    private Singleton() {
        synchronized (Singleton.class) {
            // 判断flag的值是否是true,如果是true,说明非第一次访问.直接抛一个异常
            // 如果是false,说明第一次访问,正常创建
            if (flag) {
                throw new RuntimeException("不能创建多个对象");
            }
            // 将flag的值设置为true
            flag = true;
        }
    }

    // 2.定义一个静态内部类
    private static class SingletonHolder {
        // 2.1 在内部类中声明并初始化外部类的对象
        private static final Singleton INSTANCE = new Singleton();
    }

    // 3.提供公共的访问方式
    public static Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
