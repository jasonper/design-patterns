package top.lethelh.pattern.builder.demo01;

public class Client {

    public static void main(String[] args) {
        // 1.创建指挥者对象
        Director director = new Director(new MobileBuilder());
        // 2.让指挥者指挥组装自行车
        Bike bike = director.contruct();

        System.out.println(bike.getFrame());
        System.out.println(bike.getSeat());
    }
}
