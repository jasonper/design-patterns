package top.lethelh.pattern.builder.demo01;

/**
 * 指挥者类
 */
public class Director {

    // 声明Builder类型的变量
    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    // 组装自行车的方法
    public Bike contruct() {
        builder.buildFrame();
        builder.buildSeat();
        return builder.createBike();
    }
}
