package top.lethelh.pattern.builder.demo02;

public class Client {

    public static void main(String[] args) {
        // 创建手机对象 通过构建者对象获取手机对象
        Phone phone = new Phone.Builder()
                .cpu("Intel")
                .memory("三星")
                .screen("海信")
                .mainboard("华硕")
                .build();
        System.out.println(phone);
    }
}
