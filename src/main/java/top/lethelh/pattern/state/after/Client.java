package top.lethelh.pattern.state.after;

/**
 * 状态模式测试类
 */
public class Client {

    public static void main(String[] args) {
        // 创建环境对象
        Context context = new Context();
        // 设置当前电梯状态
        context.setLiftState(new RunningState());

        context.open();
        context.close();
        context.run();
        context.stop();
    }
}
