package top.lethelh.pattern.memento.black_box;

import top.lethelh.pattern.memento.white_box.RoleStateMemento;

/**
 * 备忘录对象管理对象
 */
public class RoleStateCaretaker {

    // 声明RoleStateMemento类型变量
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
