package top.lethelh.pattern.visitor;

/**
 * 访问者模式测试
 */
public class Client {

    public static void main(String[] args) {
        // 创建Home对象
        Home home = new Home();
        // 添加元素到Home对象
        home.add(new Dog());
        home.add(new Cat());
        // 创建一个主人对象
        Owner owner = new Owner();
        // 让主人喂食所有的宠物
        home.action(owner);
    }
}
