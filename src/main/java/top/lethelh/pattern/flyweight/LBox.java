package top.lethelh.pattern.flyweight;

/**
 * L图像类(具体享元角色)
 */
public class LBox extends AbstractBox{
    @Override
    public String getShape() {
        return "L";
    }
}
