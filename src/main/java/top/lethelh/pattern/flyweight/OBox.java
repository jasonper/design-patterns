package top.lethelh.pattern.flyweight;

/**
 * O图像类(具体享元角色)
 */
public class OBox extends AbstractBox{
    @Override
    public String getShape() {
        return "O";
    }
}
