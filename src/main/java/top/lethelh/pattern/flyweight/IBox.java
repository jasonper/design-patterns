package top.lethelh.pattern.flyweight;

/**
 * I图像类(具体享元角色)
 */
public class IBox extends AbstractBox{
    @Override
    public String getShape() {
        return "I";
    }
}
