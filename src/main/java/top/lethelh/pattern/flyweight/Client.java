package top.lethelh.pattern.flyweight;

public class Client {

    public static void main(String[] args) {
        // 获取I图形
        AbstractBox box1 = BoxFactory.getInstance().getShape("I");
        box1.disply("灰色");

        // 获取L图形
        AbstractBox box2 = BoxFactory.getInstance().getShape("L");
        box2.disply("绿色");

        // 获取O图形
        AbstractBox box3 = BoxFactory.getInstance().getShape("O");
        box3.disply("灰色");

        AbstractBox box4 = BoxFactory.getInstance().getShape("O");
        box4.disply("红色");

        System.out.println("两次获取到的O图形对象是否是同一个对象:" + (box3 == box4));

    }
}
