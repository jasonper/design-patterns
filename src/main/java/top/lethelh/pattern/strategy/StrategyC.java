package top.lethelh.pattern.strategy;

/**
 * 具体策略类,封装算法
 */
public class StrategyC implements Strategy{
    @Override
    public void show() {
        System.out.println("满100元加1元换购任意200元以下产品");
    }
}
