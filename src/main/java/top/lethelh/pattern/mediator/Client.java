package top.lethelh.pattern.mediator;

/**
 * 中介者模式测试类
 */
public class Client {

    public static void main(String[] args) {
        // 创建中介者对象
        MediatorStruct mediator = new MediatorStruct();

        // 创建租房者对象
        Tenant tenant = new Tenant("李四", mediator);
        // 创建房主对象
        HouseOwner houseOwner = new HouseOwner("张三", mediator);

        // 中介者要知道具体的房主和租房者
        mediator.setTenant(tenant);
        mediator.setHouseOwner(houseOwner);

        tenant.contact("我想租两室一厅的房子");
        houseOwner.contact("我这里有两室一厅的房子,你要租吗?");
    }
}
