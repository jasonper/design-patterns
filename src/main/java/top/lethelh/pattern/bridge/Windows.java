package top.lethelh.pattern.bridge;

/**
 * 扩展抽象化角色(windows操作系统)
 */
public class Windows extends OpratingSystem{

    public Windows(VideoFile videoFile) {
        super(videoFile);
    }

    @Override
    public void play(String fileName) {
        videoFile.decode(fileName);
    }
}
