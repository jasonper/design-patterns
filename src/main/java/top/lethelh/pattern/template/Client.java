package top.lethelh.pattern.template;

public class Client {

    public static void main(String[] args) {
        // 炒包菜
        ConcreteClass_BaoCai cbc = new ConcreteClass_BaoCai();
        cbc.cookProcess();

        System.out.println("===================");

        // 炒菜心
        ConcreteClass_CaiXin ccx = new ConcreteClass_CaiXin();
        ccx.cookProcess();
    }
}
