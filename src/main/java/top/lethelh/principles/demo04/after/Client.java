package top.lethelh.principles.demo04.after;

public class Client {

    public static void main(String[] args) {
        LetheLHSafetyDoor door = new LetheLHSafetyDoor();
        door.antiTheft();
        door.fireProof();
        door.waterProof();
    }
}
