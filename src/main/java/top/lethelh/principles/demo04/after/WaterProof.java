package top.lethelh.principles.demo04.after;

/**
 * 防水接口
 */
public interface WaterProof {

    void waterProof();
}
