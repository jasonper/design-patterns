package top.lethelh.principles.demo04.before;

/**
 * 安全门接口
 */
public interface SafetyDoor {

    // 防盗功能
    void antiTheft();

    // 防火
    void fireProof();

    // 防水
    void waterProof();
}
