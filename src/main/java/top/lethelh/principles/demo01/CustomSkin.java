package top.lethelh.principles.demo01;

/**
 * 用户自定义皮肤
 */
public class CustomSkin extends AbstractSkin{
    @Override
    public void display() {
        System.out.println("用户自定义皮肤");
    }
}
