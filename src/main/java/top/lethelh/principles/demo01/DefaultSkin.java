package top.lethelh.principles.demo01;

/**
 * 默认皮肤类
 */
public class DefaultSkin extends AbstractSkin{

    @Override
    public void display() {
        System.out.println("默认皮肤");
    }
}
