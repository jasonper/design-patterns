package top.lethelh.principles.demo01;

/**
 * 抽象皮肤类
 */
public abstract class AbstractSkin {

    // 显示的方法
    public abstract void display();
}
