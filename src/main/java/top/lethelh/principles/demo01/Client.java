package top.lethelh.principles.demo01;

public class Client {

    // 开闭原则
    // 对扩展开放，对修改关闭。在程序需要进行拓展的时候，不能去修改原有的代码，实现一个热插拔的效果。简言之，是为了使程序的扩展性好，易于维护和升级。
    // 想要达到这样的效果，我们需要使用接口和抽象类。
    // 因为抽象灵活性好，适应性广，只要抽象的合理，可以基本保持软件架构的稳定。而软件中易变的细节可以从抽象派生来的实现类来进行扩展，当软件需要发生变化时，只需要根据需求重新派生一个实现类来扩展就可以了。

    public static void main(String[] args) {
        // 1.创建搜狗输入法对象
        SougouInput input = new SougouInput();

        // 2.创建皮肤对象
        // DefaultSkin defaultSkin = new DefaultSkin();
        CustomSkin customSkin = new CustomSkin();

        // 3.将皮肤设置到输入法中
        // input.setSkin(defaultSkin);
        input.setSkin(customSkin);

        // 4.展示皮肤
        // defaultSkin.display();
        input.display();
    }
}
