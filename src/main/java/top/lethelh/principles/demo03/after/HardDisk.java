package top.lethelh.principles.demo03.after;

/**
 * 硬盘接口
 */
public interface HardDisk {

    // 存储数据的方法
    public void save(String data);

    // 获取数据的方法
    public String get();
}
