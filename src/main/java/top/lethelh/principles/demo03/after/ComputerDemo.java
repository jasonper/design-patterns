package top.lethelh.principles.demo03.after;

public class ComputerDemo {

    public static void main(String[] args) {

        // 创建计算机的组件对象
        HardDisk hardDisk = new XiJieHardDisk();
        Cpu cpu = new AmdCpu();
        Memory memory = new KingstonMemory();

        // 创建计算机对象
        Computer c = new Computer();

        // 组装计算机
        c.setCpu(cpu);
        c.setHardDisk(hardDisk);
        c.setMemory(memory);

        c.run();
    }
}
