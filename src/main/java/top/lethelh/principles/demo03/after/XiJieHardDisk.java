package top.lethelh.principles.demo03.after;

public class XiJieHardDisk implements HardDisk{
    @Override
    public void save(String data) {
        System.out.println("使用希捷硬盘存储的数据为:" + data);
    }

    @Override
    public String get() {
        System.out.println("使用希捷硬盘存储的数据");
        return "数据";
    }
}
