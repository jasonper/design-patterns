package top.lethelh.principles.demo05;

/**
 * 媒体公司
 */
public class Company {

    private String name;

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
