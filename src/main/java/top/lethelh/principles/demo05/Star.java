package top.lethelh.principles.demo05;

/**
 * 明星类
 */
public class Star {

    private String name;

    public Star(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
