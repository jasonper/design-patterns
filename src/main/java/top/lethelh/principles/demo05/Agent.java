package top.lethelh.principles.demo05;

/**
 * 经纪人类
 */
public class Agent {

    // 明星
    private Star star;

    // 粉丝
    private Fans fans;

    // 媒体公司
    private Company company;

    public void setStar(Star star) {
        this.star = star;
    }

    public void setFans(Fans fans) {
        this.fans = fans;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    // 和粉丝见面的方法
    public void meeting()
    {
        System.out.println(star.getName() + "和粉丝" + fans.getName() + "见面");
    }

    // 和媒体公司洽谈的方法
    public void business()
    {
        System.out.println(star.getName() + "和" + company.getName() + "洽谈");
    }
}
